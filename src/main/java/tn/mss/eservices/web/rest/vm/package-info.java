/**
 * View Models used by Spring MVC REST controllers.
 */
package tn.mss.eservices.web.rest.vm;
