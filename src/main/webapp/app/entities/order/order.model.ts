import dayjs from 'dayjs/esm';
import { ICustomer } from 'app/entities/customer/customer.model';
import { IOrderLine } from 'app/entities/order-line/order-line.model';

export interface IOrder {
  id?: number;
  amount?: number | null;
  createdTime?: dayjs.Dayjs | null;
  customer?: ICustomer | null;
  lines?: IOrderLine[] | null;
}

export class Order implements IOrder {
  constructor(
    public id?: number,
    public amount?: number | null,
    public createdTime?: dayjs.Dayjs | null,
    public customer?: ICustomer | null,
    public lines?: IOrderLine[] | null
  ) {}
}

export function getOrderIdentifier(order: IOrder): number | undefined {
  return order.id;
}
